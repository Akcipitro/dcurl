# DigitalOcean API cURLs Simplified
# Save yourself some keystrokes
Assume a GET request unless a different type is passed in with -t
## Prereqs
- [JQ](https://stedolan.github.io/jq/manual/v1.5/)
- [DigitalOcean account](https://www.digitalocean.com/)
- [DigitalOcean API access](https://www.digitalocean.com/community/tutorials/how-to-use-the-digitalocean-api-v2)
## Install
Set dcurl to be executable:
```bash
chmod +x dcurl
```
Add dcurl to your path:
```bash
mv dcurl /usr/local/bin/dcurl
```
Set the $DO_TOKEN env variable to your API token:
```bash
echo 'export DO_TOKEN=<your token here' >> $HOME/.bashrc
source $HOME/.bashrc
```
### Examples
To get all associated Droplets:
```bash
dcurl -e droplets
```
To get a list of regions where DigitalOcean offers servers:
```bash
dcurl -e regions
```
To launch a new Droplet:
```bash
dcurl -t POST -e droplets -d '{"name":"dcurlTest","region":"nyc1","size":"1gb","image":"fedora-27-x64"}'
```
To delete a Droplet:
```bash
dcurl -t DELETE -e droplets/<droplet_id>
```
